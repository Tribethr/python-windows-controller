import win32api as wa
import win32con as wc


def buttonDown(key):
    wa.keybd_event(key,0 ,0 ,0)

def buttonUp(key):
    wa.keybd_event(key,0 ,wc.KEYEVENTF_KEYUP ,0)

def buttonClick(key):
    wa.keybd_event(key,0 ,0 ,0)
    wa.keybd_event(key,0 ,wc.KEYEVENTF_KEYUP ,0)

def setCursorAtPos(x,y):
    wa.SetCursorPos((x,y))

def mouseClick(x,y):
    wa.SetCursorPos((x,y))
    wa.mouse_event(wc.MOUSEEVENTF_LEFTDOWN,x,y,0,0)
    wa.mouse_event(wc.MOUSEEVENTF_LEFTUP,x,y,0,0)


